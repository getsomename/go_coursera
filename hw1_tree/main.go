package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

type PrefixWriter struct {
	Prefix string
	Writer io.Writer
}

func NewPrefix(prefix string, out io.Writer) io.Writer {
	return &PrefixWriter{prefix, out}
}

func (pw PrefixWriter) Write(p []byte) (n int, err error) {
	return pw.Writer.Write(append([]byte(pw.Prefix)[:], p[:]...))
}

func main() {
	if len(os.Args) < 2 || len(os.Args) > 3 {
		log.Fatalf("usage go run main.go . [-f]")
	}
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(os.Stdout, os.Args[1], printFiles)
	if err != nil {
		log.Fatalf("failed while running dirTree: %s", err)
	}
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}
	if !printFiles {
		files = filterDirs(files)
	}
	var filePrefix, dirPrefix string
	lastIdx := len(files) - 1
	for i, file := range files {
		if i == lastIdx {
			filePrefix, dirPrefix = "└", "\t"
		} else {
			filePrefix, dirPrefix = "├", "│\t"
		}
		if _, err = fmt.Fprintf(out, "%s───%s\n", filePrefix, wrapName(file)); err != nil {
			return err
		}
		if !file.IsDir() {
			continue
		}
		if err = dirTree(NewPrefix(dirPrefix, out), path+"/"+file.Name(), printFiles); err != nil {
			return err
		}
	}
	return nil
}

func filterDirs(files []os.FileInfo) []os.FileInfo {
	result := make([]os.FileInfo, 0, len(files))
	for _, file := range files {
		if file.IsDir() {
			result = append(result, file)
		}
	}
	return result
}

func wrapName(file os.FileInfo) string {
	var suffix string
	if !file.IsDir() {
		suffix = getSizeStr(file)
	}
	return fmt.Sprintf("%s%s", file.Name(), suffix)
}

func getSizeStr(file os.FileInfo) string {
	if file.Size() != 0 {
		return fmt.Sprintf(" (%db)", file.Size())
	}
	return " (empty)"
}
